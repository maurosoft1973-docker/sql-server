#!/bin/bash

# Run the setup script to clean all object from schema in the DB
/opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P $SA_PASSWORD -d $MSSQL_DB -i clean-db.sql