#!/bin/bash
SCRIPT=""

# Loop through arguments and process them
for arg in "$@"
do
    case $arg in
        -s=*|--script==*)
        SCRIPT="${arg#*=}"
        shift # Remove
        ;;
        -h|--help)
        echo -e "usage "
        echo -e "$0 "
        echo -e "  -s|--script   -> ${SCRIPT:=""} (path of script)"
        exit 0
        ;;
    esac
done

# check argument
if [ "$SCRIPT" = "" ]; then
    echo "Argument script missing"
    exit 0    
fi

# Check file script
if [ -f "$SCRIPT" ]; then
    # Run the script
    /opt/mssql-tools/bin/sqlcmd -S localhost -U sa -P $SA_PASSWORD -d $MSSQL_DB -i $SCRIPT
else
    echo "The path of script $SCRIPT does not exist"
fi
