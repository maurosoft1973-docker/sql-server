ARG SQL_RELEASE

FROM mcr.microsoft.com/mssql/server:$SQL_RELEASE

ARG BUILD_DATE

LABEL \
    maintainer="Mauro Cardillo <mauro.cardillo@gmail.com>" \
    build="$BUILD_DATE" \
    org.opencontainers.image.title="alpine-net" \
    org.opencontainers.image.description="Sql Server $SQL_RELEASE Docker image" \
    org.opencontainers.image.authors="Mauro Cardillo <mauro.cardillo@gmail.com>" \
    org.opencontainers.image.vendor="Mauro Cardillo" \
    org.opencontainers.image.version="v$SQL_RELEASE" \
    org.opencontainers.image.url="https://hub.docker.com/r/maurosoft1973/sql-server" \
    org.opencontainers.image.source="https://gitlab.com/maurosoft1973-docker/sql-server" \
    org.opencontainers.image.created=$BUILD_DATE


# Create a config directory
RUN mkdir -p /usr/config
WORKDIR /usr/config

# Bundle config source
COPY . /usr/config

# Grant permissions for to our scripts to be executable
RUN chmod +x /usr/config/entrypoint.sh
RUN chmod +x /usr/config/create-db.sh
RUN chmod +x /usr/config/clean-db.sh
RUN chmod +x /usr/config/run-scriptsql.sh

ENTRYPOINT ["./entrypoint.sh"]

# Tail the setup logs to trap the process
CMD ["tail -f /dev/null"]

HEALTHCHECK --interval=60s CMD /opt/mssql-tools/bin/sqlcmd -U sa -P $SA_PASSWORD -Q "select 1" && grep -q "MSSQL CONFIG COMPLETE" ./config.log