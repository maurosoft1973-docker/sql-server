# Sql Server Docker image running

This Docker image [(https://hub.docker.com/r/maurosoft1973/sql-server)](https://hub.docker.com/r/maurosoft1973/sql-server/) is based on the minimal [Sql Server](https://hub.docker.com/_/microsoft-mssql-server/)

##### Sql Release %SQL_RELEASE%

----

## Description

This image includes the automatic creation database and user


## Architectures

* ```:x86_64```  - 64 bit Intel/AMD (x86_64/amd64)

## Tags

* ```:latest```         latest branch based
* ```:test```           test branch based
* ```:%SQL_RELEASE%```  %SQL_RELEASE% branch based

## Environment Variables:

### Main Sql Server parameters:
* `ACCEPT_EULA`: Y
* `MSSQL_AGENT_ENABLED`: true enabled, false disabled
* `MSSQL_SA_PASSWORD`: Password for sa user
* `MSSQL_DB`: remote host address ip
* `MSSQL_USER`: user for access to remote host
* `MSSQL_PASSWORD`: user password for access to remote host

***
###### Last Update %LAST_UPDATE%
